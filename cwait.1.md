CWAIT(1) - General Commands Manual

# NAME

**cwait** - Start & modify timeouts, with visual feedback

# SYNOPSIS

**cwait**
\[intervals...]
\[-e&nbsp;command]

# DESCRIPTION

The
**cwait**
utility sleeps for a given amount of time specified by the interval list while
giving a visual feedback of the remaining time. The interval list is a sequence
of number (integer) followed by 's', 'm', 'h' or 'd' respectively for seconds,
minutes, hours, and days. The order does not matter and the duplicates will be
summed (i.e. "3s 3s" is 6 seconds).

While running the utility will print out the remaining time and will accept
commands to edit the timeout.

# OPTIONS

The only option accepted is:

**-e**

> Specifies the command to be executed at the end of the timeout

# KEYS

Is possible to interact with
**cwait**
with the following key:

p

> Pause or unpause the timer

d

> Add a day

D

> Remove a day

h

> Add an hour

H

> Remove an hour

m

> Add a minute

m

> Remove a minute

s

> Add a second

S

> Remove a second

# ENVIRONMENT

The command given is passed directly to
execvp(3)
so it'll be searched on the
`$PATH`.

# EXAMPLES

Sleep for one hour, ten minutes and twelve seconds and then display a notification

	cwait 1h 10m 12s -e notify-send 'Remeber to take a break!'

# SEE ALSO

sleep(1)

# AUTHORS

Omar Polo &lt;omar.polo@europecom.net&gt;

OpenBSD 6.3 - June 22, 2018
