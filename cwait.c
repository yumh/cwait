#include <sys/select.h>

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <termios.h>
#include <unistd.h>

void
clearlen(int len) {
	while (len--)
		putchar(' ');
}

long
tosecs(char *s) {
	long	n;
	int	len;
	char	c;

	len = strlen(s);

	if (len == 1) {
		fprintf(stderr, "Invalid interval value \"%s\"\n", s);
		exit(EX_USAGE);
	}

	c = s[len -1];
	s[len -1] = 0;

	errno = 0;
	n = strtol(s, NULL, 10);

	if (errno != 0) {
		fprintf(stderr, "Invalid number \"%s\"\n", s);
		exit(EX_USAGE);
	}

	switch (c) {
		case 's':
			return n;
		case 'm':
			return n * 60;
		case 'h':
			return n * 60 * 60;
		case 'd':
			return n * 60 * 60 * 24;
		default:
			fprintf(stderr, "Invalid unit \"%c\"\n", c);
			exit(EX_USAGE);
	}
}

char
*secs2str(long secs) {
	long	days, hours, minutes;
	char 	*r;

	/* count days */
	days = secs / (24 * 60 * 60);
	secs -= days * 24 * 60 * 60;

	/* count hours */
	hours = secs / (60 * 60);
	secs -= hours * 60 * 60;

	/* count minutes */
	minutes = secs / 60;
	secs -= minutes * 60;

	if (days != 0)
		asprintf(&r, "%ldd %ldh %ldm %lds!", days, hours, minutes, secs);
	else if (hours != 0)
		asprintf(&r, "%ldh %ldm %lds!", hours, minutes, secs);
	else if (minutes != 0)
		asprintf(&r, "%ldm %lds!", minutes, secs);
	else
		asprintf(&r, "%lds!", secs);

	return r;
}

void
help(char *prgname) {
	printf("Usage: %s intervals... [-e command]\n", prgname);
	printf("\n");
	printf(" Where intervals is a list of number followed by 's', 'm', 'h', 'd' for seconds, minutes, hours or days. Order does not count.\n");
	printf("\n");
	printf(" For example '3s 3s 1h 4m' is perfectly valid (1 hour, 4 minutes and 6 seconds)\n");
	printf("\n");
	printf(" During the execution you can press:\n");
	printf(" - 'p' to pause/unpause\n");
	printf(" - 'd' to add a day\n");
	printf(" - 'D' to remove a day\n");
	printf(" - 'h' to add an hour\n");
	printf(" - 'H' to remove an hour\n");
	printf(" - 'm' to add a minute\n");
	printf(" - 'M' to remove a minute\n");
	printf(" - 's' to add a second\n");
	printf(" - 'S' to remove a second\n");
}

void
icrem(long *s, long t) {
	if (*s >= t)
		*s -= t;
}

void
handle_next_char(long *s, int *p) {
	switch (getchar()) {
		case 'p': /* pause */
			*p = !*p;
			break;
		case 'd': /* add one day */
			*s += 24 * 60 * 60;
			break;
		case 'D': /* remove one day */
			icrem(s, 24 * 60 * 60);
			break;
		case 'h': /* add one hour */
			*s += 60 * 60;
			break;
		case 'H': /* remove one hour */
			icrem(s, 60 * 60);
			break;
		case 'm': /* add one minute */
			*s += 60;
			break;
		case 'M': /* remove one minute */
			icrem(s, 60);
			break;
		case 's': /* add one second */
			*s += 1;
			break;
		case 'S': /* remove one second */
			icrem(s, 1);
			break;
	}
}

int
main(int argc, char **argv) {
	struct	termios oldt, newt;
	struct	timeval user_input_timeout;
	long	secs;
	int	with_exec, start_command, i, lastlen, pause;

	secs = 0;
	with_exec = 0;
	start_command = 0;
	lastlen = 0;
	pause = 0;

	for (i = 1; i < argc; ++i) {
		if (!strcmp(argv[i], "-e")) {
			with_exec = 1;
			start_command = i+1;
			break;
		} else if (!strcmp(argv[i], "-h")) {
			help(argv[0]);
			return 0;
		} else {
			secs += tosecs(argv[i]);
		}
	}

	/* ncurses' noecho & raw mode in a nutsell */
	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);

	user_input_timeout.tv_sec = 0;
	user_input_timeout.tv_usec = 0;

	for (; secs > 0; !pause && --secs) {
		char *c;

		fd_set readfds;
		FD_ZERO(&readfds);
		FD_SET(STDIN_FILENO, &readfds);

		while (select(1, &readfds, NULL, NULL, &user_input_timeout)) {
			/* ok - got some input, handle it and continue */
			handle_next_char(&secs, &pause);
		}

		clearlen(lastlen);

		c = secs2str(secs);
		lastlen = strlen(c);
		printf("\r%s", c);
		fflush(stdout);
		free(c);

		sleep(1);
	}

	/* clear line */
	putchar('\r');
	clearlen(lastlen);
	fflush(stdout);
	putchar('\n');

	/* restore the old settings */
	tcsetattr( STDIN_FILENO, TCSANOW, &oldt);

	if (with_exec) {
		char *msg;

		assert(start_command < argc);
		execvp(argv[start_command], argv + start_command);

		asprintf(&msg, "%s: error exec'ing %s", argv[0], argv[start_command]);
		perror(msg);
		free(msg);
		return EX_USAGE;
	}

	return 0;
}

