# cwait

Start & modify timeouts, with visual feedback

## Description

Dead-simple utility written in C to create timeouts, execute command at the end,
and manipulate such timeouts with a visual feedback.

### Differences with GNU coreutils' `sleep(1)`

The visual feedback and the ability to pause/unpause/edit the timeout while is
running.

## Dependencies

The only dependency is a C compiler and a POSIX C library with the non-standard
`asprintf(3)` function available. Since {Free,Net,Open}BSD and almost all
GNU/Linux distro that I've tried had that function I'm ok with using it. If
you're on a platform where is not present it should be trivial to write a patch
for that, for example:

```C
#define asprintf(a, ...)                     \
  ({                                         \
    /* for brevity don't check for errors */ \
    *a = calloc(15 * sizeof(char));          \
    return snprintf(*a, 15, __VA_ARGS__);    \
  })
```

The prototype for `asprintf` is:

```C
int asprintf(char **ret, const char *format, ...);
```

and behaves exactly like `sprintf(3)` but it automatically allocate memory for
the string.

## Building

```sh
$ cc cwait.c -o cwait
```
or, if you're using GNU glibc:
```sh
$ cc -D_GNU_SOURCE cwait.c -o cwait
```

## Example

```sh
$ cwait 1h 10m -e notify-send 'Take a break!'
# wait for one hour and ten minute and then exec `notify-send 'Take a break!'`

$ cwait 3s 3s 1h 5m 1h
# wait for two hour, five minutes and six seconds
```

## Documentation

See

```sh
$ cwait -h
```
or [the manpage](cwait.1.md) ([mandoc version](cwait.1)).
